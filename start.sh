#!/bin/bash

echo 'default:' > out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab1 $i >> out.txt
done
echo '' >> out.txt
echo 'two-cores:' >> out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab1-2 $i >> out.txt
done
echo '' >> out.txt
echo 'four-cores:' >> out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab1-4 $i >> out.txt
done
echo '' >> out.txt
echo 'eight-cores:' >> out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab1-8 $i >> out.txt
done
echo '' >> out.txt
echo 'sixteen-cores:' >> out.txt
for ((i = 470; i <= 12400; i += 1193)); do
    ./lab1-16 $i >> out.txt
done