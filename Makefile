build-default:
	clang -o lab1 main.c -lm

run-default: build-default
	./lab1 11543

build-two-cores:
	gcc -o3 -Wall -Werror -floop-parallelize-all -ftree-parallelize-loops=2 -o lab1-2 main.c -lm

build-four-cores:
	gcc -o3 -Wall -Werror -floop-parallelize-all -ftree-parallelize-loops=4 -o lab1-4 main.c -lm

build-eight-cores:
	gcc -o3 -Wall -Werror -floop-parallelize-all -ftree-parallelize-loops=8 -o lab1-8 main.c -lm

build-sixteen-cores:
	gcc -o3 -Wall -Werror -floop-parallelize-all -ftree-parallelize-loops=16 -o lab1-16 main.c -lm

build-lab: build-default build-two-cores build-four-cores build-eight-cores build-sixteen-cores