FROM ubuntu:20.04
LABEL name="spo"
COPY main.c .
COPY Makefile .
COPY start.sh .
RUN apt update
RUN apt install -y --allow-unauthenticated \
    gcc \
    clang \
    cmake \
    make \